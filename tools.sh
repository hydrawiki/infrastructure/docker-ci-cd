# Common variables and functions for our CI/CD tasks
#
# This is intended to run in alpine's busybox shell, a
# bash-compatible ash variant.
#
# This is intended to run on an AWS EC2 instance with an
# IAM instance profile providing appropriate permissions.
# If this is not the case, AWS_REGION should be set to
# avoid polling the instance metadata server for the
# instance's region.

# Set DOCKER_HOST if on a k8s runner
if ! docker info &>/dev/null; then
  if [ -z "$DOCKER_HOST" -a "$KUBERNETES_PORT" ]; then
    export DOCKER_HOST='tcp://localhost:2375'
  fi
fi

# Install packages for using awscli on Alpine.
function install_packages() {
  echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >>/etc/apk/repositories
  apk add --update --no-cache \
    aws-cli \
    curl \
    gettext \
    git \
    jq \
    less
}

# Initialize AWS-specific environment variables.
function init_aws() {
  # Grab account ID from caller identity if not supplied
  if [ -z "$AWS_ACCOUNT_ID" ]; then
    AWS_ACCOUNT_ID=$(aws sts get-caller-identity | jq -r .Account)
  fi

  # Grab region from instance metadata if not supplied
  if [ -z "$AWS_REGION" ]; then
    AWS_REGION=$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone | head -c-1)
  fi

  # Set AWS_DEFAULT_REGION for awscli
  export AWS_DEFAULT_REGION=$AWS_REGION
}

function docker_aws_login() {
  # Login to AWS ECR
  $(aws ecr get-login --no-include-email)
}

# Echo the hostname of the ECS repository
function docker_repository() {
  echo "$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com"
}

# Build a Docker image tagged with the current commit hash and push it to ECR.
#
# Usage: `docker_build_push hydra varnish` will build a Docker image from
# `varnish.Dockerfile` and push it to the ECR repository `hydra/varnish`.
function docker_build_push() {
  GROUP_NAME=$1 # e.g. hydra
  SERVICE_NAME=$2 # e.g. varnish

  REPOSITORY_NAME=$1/$2
  REPOSITORY_URL=$(docker_repository)/$REPOSITORY_NAME

  # Build docker image
  docker build -t $REPOSITORY_URL:$CI_COMMIT_SHA -f $SERVICE_NAME.Dockerfile .

  # Push to ECR
  docker push $REPOSITORY_URL:$CI_COMMIT_SHA

  # Push the tag, if any
  if [ -n "$CI_COMMIT_TAG" ]; then
    docker tag $REPOSITORY_URL:$CI_COMMIT_SHA $REPOSITORY_URL:$CI_COMMIT_TAG
    docker push $REPOSITORY_URL:$CI_COMMIT_TAG
  fi
}

# Build a Docker image tagged with the current commit hash.  Does not push
# the image to ECR.
#
# Sets $BUILT_IMAGE to the docker image name and tag that was built.
function docker_build() {
  GROUP_NAME=$1 # e.g. hydra
  SERVICE_NAME=$2 # e.g. varnish

  REPOSITORY_NAME=$1/$2
  REPOSITORY_URL=$(docker_repository)/$REPOSITORY_NAME

  # Build docker image
  docker build -t $REPOSITORY_URL:$CI_COMMIT_SHA -f $SERVICE_NAME.Dockerfile .

  BUILT_IMAGE=$REPOSITORY_URL:$CI_COMMIT_SHA
}

# Deploy to an ECS service.
#
# This does not do automatic rollback, and it does not create extra resources
# to compensate for the deploying-to nodes being offline.
#
# Usage: `deploy_ecs_service hydra varnish` will update the service
# hydra-varnish in cluster hydra-varnish to use a new task definition based
# on the file ecs-task-definition.json.
function deploy_ecs_service() {
  GROUP_NAME=$1 # e.g. hydra
  SERVICE_NAME=$2 # e.g. varnish
  MIN_HEALTH_PERCENT=$3 # 0-100, minimum percentage of healthy nodes in the service during the deploy
  WAIT_TIMEOUT=$4 # time in seconds to wait for service deployment

  if [ -z "$MIN_HEALTH_PERCENT" ]; then
    MIN_HEALTH_PERCENT=0
  fi

  if [ -z "$WAIT_TIMEOUT" ]; then
    WAIT_TIMEOUT=360
  fi

  if [ -n "$DEPLOY_KEY_ID" ]; then
    export AWS_ACCESS_KEY_ID="$DEPLOY_KEY_ID"
    export AWS_SECRET_ACCESS_KEY="$DEPLOY_SECRET_KEY"
  fi

  ECS_CLUSTER_NAME="$GROUP_NAME-$SERVICE_NAME"

  # Generate the new task definition from ecs-task-definition.json
  export REGISTRY_URL=$(docker_repository)
  export IMAGE_TAG=$CI_COMMIT_SHA
  export AWSLOGS_REGION=$AWS_REGION
  envsubst '$REGISTRY_URL $IMAGE_TAG $AWSLOGS_REGION' < ecs-task-definition.json > ecs-task-definition.actual.json

  # Register the new task definition
  NEW_TASK_DEFINITION_ARN=$(aws ecs register-task-definition --cli-input-json 'file://ecs-task-definition.actual.json' | jq -r ".taskDefinition.taskDefinitionArn")
  NEW_TASK_DEFINITION=$(echo "$NEW_TASK_DEFINITION_ARN" | awk -F/ '{print $2}')

  # Update the ECS service to use the new task definition
  aws ecs update-service \
    --cluster $ECS_CLUSTER_NAME \
    --service $ECS_CLUSTER_NAME \
    --task-definition $NEW_TASK_DEFINITION \
    --deployment-configuration maximumPercent=100,minimumHealthyPercent=$MIN_HEALTH_PERCENT | jq -r '.service'

  if [ "$WAIT_TIMEOUT" == "0" ]; then
    return 0
  fi

  echo "Waiting for service deployment"
  TIMED_OUT=1
  # Show events from 5 seconds ago onward
  LAST_CREATED_AT=$((`date +%s` - 5))
  for i in `seq $(($WAIT_TIMEOUT/5))`; do
    sleep 5
    SERVICE_DESC=$(aws ecs describe-services --cluster $ECS_CLUSTER_NAME --service $ECS_CLUSTER_NAME | jq -r '.services[0]')
    echo "$SERVICE_DESC" | jq -r ".events[] | select(.createdAt > $LAST_CREATED_AT) | ((.createdAt|todate)+\" \"+.message)" | tac
    DEPLOY_STATUS=$(echo "$SERVICE_DESC" | jq -r '.deployments[] | select(.status == "PRIMARY")')
    if [ $(echo "$DEPLOY_STATUS" | jq -r .runningCount) == $(echo "$DEPLOY_STATUS" | jq -r .desiredCount) ]; then
      TIMED_OUT=0
      break
    fi
    LAST_CREATED_AT=$(echo "$SERVICE_DESC" | jq -r '.events[0].createdAt')
    echo "Waiting ... $((i*5))s/${WAIT_TIMEOUT}s"
  done
  if [ "$TIMED_OUT" == "0" ]; then
    echo "Service deployment finished"
  else
    echo "Service deployment timed out"
    return 1
  fi
}
