FROM docker:stable

COPY tools.sh /usr/bin/tools.sh

RUN . tools.sh && install_packages
