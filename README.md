This is the base Docker image is used to do GitLab CI builds and deploys in our
AWS infrastructure.

It installs a few packages, notably aws-cli, and it imports a shell script
library for use in CI tasks.

## Usage

In `.gitlab-ci.yml`, set `image` to
`516092867728.dkr.ecr.us-east-1.amazonaws.com/ci/tools:{version}` to use this
image.  Then, in `before_script`, `. tools.sh` to import the shell script
library.

For a working example, see Varnish's [`.gitlab-ci.yml`](https://gitlab.com/hydrawiki/infrastructure/varnish/blob/master/.gitlab-ci.yml).

```yaml
image: 516092867728.dkr.ecr.us-east-1.amazonaws.com/ci/tools:1.0.3

...

before_script:
- . tools.sh
- init_aws
- docker_aws_login
```

## Library Functions

#### `init_aws`

`init_aws` sets `$AWS_ACCOUNT_ID` and `$AWS_REGION` if they are not set, and it exports `$AWS_DEFAULT_REGION`.
When unset, it gets `$AWS_ACCOUNT_ID` from [STS](https://docs.aws.amazon.com/cli/latest/reference/sts/get-caller-identity.html),
and it gets `$AWS_REGION` from the [EC2 Instance Metadata](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-metadata.html).

#### `docker_aws_login`

`docker_aws_login` calls `aws ecr get-login` to log in to ECR with the current Docker client.
This must be ran before pushing to or pulling from ECR.

#### `docker_build_push`

`docker_build_push [group name] [service name]` builds an image and pushes it to the `/{group name}/{service name}`
ECR repository in the current account and region.  By default the image is tagged with the current commit hash.  If
CI is being ran for a git tag, the image is tagged and pushed with that tag as well.

#### `docker_build`

`docker_build [group name] [service name]` builds an image tagged as `{ECR registry host}/{group name}/{service name}`,
and returns that image name and tag as the `$BUILT_IMAGE` variable.

#### `deploy_ecs_service`

`deploy_ecs_service [group name] [service name] [minimum healthy percent] [timeout]` registers a new task definition
using the local file `ecs-task-definition.json` as a template.  The ECS service named `{group name}-{service name}`
in the cluster also named `{group name}-{service name}` is then updated to use the new task definition, and
the minimum healthy percent supplied is used to keep a minimum number of tasks running, if any.  The function will
then wait up to `[timeout]` seconds for the deployment to complete.  Timeout defaults to 360.  If timeout is 0, the
deploy will not be waited upon.

Template substitution is performed with [`envsubst`](https://www.gnu.org/software/gettext/manual/html_node/envsubst-Invocation.html).

Template variables:
- `REGISTRY_URL`: The hostname of the Docker registry which contains the image, e.g. `516092867728.dkr.ecr.us-east-1.amazonaws.com`.
- `IMAGE_TAG`: The commit hash (`$CI_COMMIT_HASH`) for the current job.
- `AWSLOGS_REGION`: The current region, used for setting the [awslogs container log driver's](https://docs.docker.com/config/containers/logging/awslogs/) options.

#### `docker_repository`

`docker_repository` echoes the hostname of the ECS repository, e.g. `516092867728.dkr.ecr.us-east-1.amazonaws.com`.

#### `install_packages`

`install_packages` is used only in this package's Docker build process.
It does not need to be called by users of the image.
